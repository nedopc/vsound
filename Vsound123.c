#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#if 0
#define STDOUT
#endif

#if 1

/* Digital filter designed by mkfilter/mkshape/gencode   A.J. Fisher
   Command line: /www/usr/fisher/helpers/mkfilter -Bu -Lp -o 3 -a 1.9954648526e-03 0.0000000000e+00 -l */

#define NZEROS 3
#define NPOLES 3
#define GAIN   4.110041734e+06

double xv[40][NZEROS+1], yv[40][NPOLES+1];

static double filterstep(double v, int i)
{
        xv[i][0] = xv[i][1]; xv[i][1] = xv[i][2]; xv[i][2] = xv[i][3];
        xv[i][3] = v / GAIN;
        yv[i][0] = yv[i][1]; yv[i][1] = yv[i][2]; yv[i][2] = yv[i][3];
        yv[i][3] =   (xv[i][0] + xv[i][3]) + 3 * (xv[i][1] + xv[i][2])
                     + (  0.9752358741 * yv[i][0]) + ( -2.9501622338 * yv[i][1])
                     + (  2.9749244133 * yv[i][2]);
        return yv[i][3];
}

#else

/* Digital filter designed by mkfilter/mkshape/gencode   A.J. Fisher
   Command line: /www/usr/fisher/helpers/mkfilter -Bu -Lp -o 3 -a 1.1337868481e-03 0.0000000000e+00 -l */

#define NZEROS 3
#define NPOLES 3
#define GAIN   2.228663279e+07

static double xv[40][NZEROS+1], yv[40][NPOLES+1];

static double filterstep(double v, int i)
{
        xv[i][0] = xv[i][1]; xv[i][1] = xv[i][2]; xv[i][2] = xv[i][3];
        xv[i][3] = v / GAIN;
        yv[i][0] = yv[i][1]; yv[i][1] = yv[i][2]; yv[i][2] = yv[i][3];
        yv[i][3] =   (xv[i][0] + xv[i][3]) + 3 * (xv[i][1] + xv[i][2])
                     + (  0.9858534011 * yv[i][0]) + ( -2.9716062045 * yv[i][1])
                     + (  2.9857524444 * yv[i][2]);
        return yv[i][3];
}

#endif

/* END of low-pass filter (it was modified to be 40 identical filters) */

void cleanfilters(void)
{
  int i,j;
  for(i=0;i<40;i++)
  {
     for(j=0;j<=NZEROS;j++)
     {
       xv[i][j] = 0.0;
       yv[i][j] = 0.0;
     }
  }
}

#ifndef STDOUT
#include <ncurses.h>
#endif

double freq[] = {
/*0:*/   110.000000,
/*1:*/   141.274739,
/*2:*/   181.441381,
/*3:*/   233.028034,
/*4:*/   299.281588,
/*5:*/   384.372074,
/*6:*/   493.655131,
/*7:*/   634.009089,
/*8:*/   814.267897,
/*9:*/  1045.777132,
/*10:*/ 1343.108104,
/*11:*/ 1724.974971,
/*12:*/ 2215.412625,
/*13:*/ 2845.289458,
/*14:*/ 3654.250232,
/*15:*/ 4693.211343,
/*16:*/ 6027.565523,
/*17:*/ 7741.297691,
/*18:*/ 9942.271007,
/*19:*/12769.015831
};

#define SAMPLERATE 44100

double sin00[SAMPLERATE];
double cos00[SAMPLERATE];
double sin01[SAMPLERATE];
double cos01[SAMPLERATE];
double sin02[SAMPLERATE];
double cos02[SAMPLERATE];
double sin03[SAMPLERATE];
double cos03[SAMPLERATE];
double sin04[SAMPLERATE];
double cos04[SAMPLERATE];
double sin05[SAMPLERATE];
double cos05[SAMPLERATE];
double sin06[SAMPLERATE];
double cos06[SAMPLERATE];
double sin07[SAMPLERATE];
double cos07[SAMPLERATE];
double sin08[SAMPLERATE];
double cos08[SAMPLERATE];
double sin09[SAMPLERATE];
double cos09[SAMPLERATE];
double sin10[SAMPLERATE];
double cos10[SAMPLERATE];
double sin11[SAMPLERATE];
double cos11[SAMPLERATE];
double sin12[SAMPLERATE];
double cos12[SAMPLERATE];
double sin13[SAMPLERATE];
double cos13[SAMPLERATE];
double sin14[SAMPLERATE];
double cos14[SAMPLERATE];
double sin15[SAMPLERATE];
double cos15[SAMPLERATE];
double sin16[SAMPLERATE];
double cos16[SAMPLERATE];
double sin17[SAMPLERATE];
double cos17[SAMPLERATE];
double sin18[SAMPLERATE];
double cos18[SAMPLERATE];
double sin19[SAMPLERATE];
double cos19[SAMPLERATE];

char convert2ascii(double da)
{
   /* 8 levels: .:-=#%@ should represent amplitude 0...1638 */
   char c; /* 1638/8~205 and input is amplitude squared so */
   if(da < 42025)        c = ' '; /* (1*205)^2 */
   else if(da < 168100)  c = '.'; /* (2*205)^2 */
   else if(da < 378225)  c = ':'; /* (3*205)^2 */
   else if(da < 672400)  c = '-'; /* (4*205)^2 */
   else if(da < 1050625) c = '='; /* (5*205)^2 */
   else if(da < 1512900) c = '#'; /* (6*205)^2 */
   else if(da < 2059225) c = '%'; /* (7*205)^2 */
   else c = '@'; /* (8*205)^2 = 2689600 is theoretical max */
   return c;
}

char filename[256] = "vsound.raw";

#ifdef STDOUT
#define AWINDOW 126
#else
#define AWINDOW 63
#endif

double av[20][AWINDOW];

int main()
{
 FILE *f;
 char c;
 short w;
 int i,j,k,a,u1,u2,o1=0,o2=0,oo2=0,nx=0,frame=0,ms=0;
 long l,sz;
 clock_t t;
 double d,ds,dc,ot=0,coef[20],m=1.414;

 for(i=0;i<20;i++)
 {
    coef[i] = 2*M_PI/SAMPLERATE*freq[i];
 }

 for(i=0;i<SAMPLERATE;i++)
 {
   sin00[i] = m*sin(i*coef[0]);
   sin01[i] = m*sin(i*coef[1]);
   sin02[i] = m*sin(i*coef[2]);
   sin03[i] = m*sin(i*coef[3]);
   sin04[i] = m*sin(i*coef[4]);
   sin05[i] = m*sin(i*coef[5]);
   sin06[i] = m*sin(i*coef[6]);
   sin07[i] = m*sin(i*coef[7]);
   sin08[i] = m*sin(i*coef[8]);
   sin09[i] = m*sin(i*coef[9]);
   sin10[i] = m*sin(i*coef[10]);
   sin11[i] = m*sin(i*coef[11]);
   sin12[i] = m*sin(i*coef[12]);
   sin13[i] = m*sin(i*coef[13]);
   sin14[i] = m*sin(i*coef[14]);
   sin15[i] = m*sin(i*coef[15]);
   sin16[i] = m*sin(i*coef[16]);
   sin17[i] = m*sin(i*coef[17]);
   sin18[i] = m*sin(i*coef[18]);
   sin19[i] = m*sin(i*coef[19]);
   cos00[i] = m*cos(i*coef[0]);
   cos01[i] = m*cos(i*coef[1]);
   cos02[i] = m*cos(i*coef[2]);
   cos03[i] = m*cos(i*coef[3]);
   cos04[i] = m*cos(i*coef[4]);
   cos05[i] = m*cos(i*coef[5]);
   cos06[i] = m*cos(i*coef[6]);
   cos07[i] = m*cos(i*coef[7]);
   cos08[i] = m*cos(i*coef[8]);
   cos09[i] = m*cos(i*coef[9]);
   cos10[i] = m*cos(i*coef[10]);
   cos11[i] = m*cos(i*coef[11]);
   cos12[i] = m*cos(i*coef[12]);
   cos13[i] = m*cos(i*coef[13]);
   cos14[i] = m*cos(i*coef[14]);
   cos15[i] = m*cos(i*coef[15]);
   cos16[i] = m*cos(i*coef[16]);
   cos17[i] = m*cos(i*coef[17]);
   cos18[i] = m*cos(i*coef[18]);
   cos19[i] = m*cos(i*coef[19]);
 }

 cleanfilters();

#ifndef STDOUT
 initscr();
#endif

 f = fopen("vsound.raw","rb");
 if(f==NULL) return -1;
 fseek(f,0,SEEK_END);
 sz = ftell(f);
 fseek(f,0,SEEK_SET);
 t = clock();
 a = k = 0;
 for(l=0;l<sz;l+=2)
 {
   w = fgetc(f);
   w |= fgetc(f)<<8;
   d = w;

   dc = filterstep(d*cos00[k],0);
   ds = filterstep(d*sin00[k],1);
   av[0][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos01[k],2);
   ds = filterstep(d*sin01[k],3);
   av[1][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos02[k],4);
   ds = filterstep(d*sin02[k],5);
   av[2][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos03[k],6);
   ds = filterstep(d*sin03[k],7);
   av[3][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos04[k],8);
   ds = filterstep(d*sin04[k],9);
   av[4][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos05[k],10);
   ds = filterstep(d*sin05[k],11);
   av[5][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos06[k],12);
   ds = filterstep(d*sin06[k],13);
   av[6][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos07[k],14);
   ds = filterstep(d*sin07[k],15);
   av[7][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos08[k],16);
   ds = filterstep(d*sin08[k],17);
   av[8][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos09[k],18);
   ds = filterstep(d*sin09[k],19);
   av[9][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos10[k],20);
   ds = filterstep(d*sin10[k],21);
   av[10][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos11[k],22);
   ds = filterstep(d*sin11[k],23);
   av[11][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos12[k],24);
   ds = filterstep(d*sin12[k],25);
   av[12][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos13[k],26);
   ds = filterstep(d*sin13[k],27);
   av[13][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos14[k],28);
   ds = filterstep(d*sin14[k],29);
   av[14][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos15[k],30);
   ds = filterstep(d*sin15[k],31);
   av[15][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos16[k],32);
   ds = filterstep(d*sin16[k],33);
   av[16][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos17[k],34);
   ds = filterstep(d*sin17[k],35);
   av[17][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos18[k],36);
   ds = filterstep(d*sin18[k],37);
   av[18][a] = dc*dc + ds*ds;

   dc = filterstep(d*cos19[k],38);
   ds = filterstep(d*sin19[k],39);
   av[19][a] = dc*dc + ds*ds;

   if(++k >= SAMPLERATE) k=0;
   if(++a >= AWINDOW)
   {
     a = 0;
#ifdef STDOUT
     printf("%09d|",(int)(l/2));
#endif
     u1 = u2 = 1;
     for(i=0;i<20;i++)
     {
       d = 0;
       for(j=0;j<AWINDOW;j++)
       {
         d += av[i][j];
       }
       c = convert2ascii(d/AWINDOW);
       if(c!=' ') u1=0; /* it is not condition 1 */
       if(c==' ') u2=0; /* it is not condition 2 */
#ifdef STDOUT
       printf("%c",c);
#else
       mvaddch(20-i,nx,c);
#endif
     }
#ifndef STDOUT
     mvaddch(0,nx,' ');
#endif
     nx++;
     if(o1&&u1) ot=l*500.0/SAMPLERATE;
     if((oo2&&o2&&u2&&(l*500.0/SAMPLERATE-ot)<=13&&nx>2000/AWINDOW)||nx>SAMPLERATE/AWINDOW/4)
     {
       c = '<';
       ms = nx*AWINDOW*1000/SAMPLERATE;
       ot = 0;
       nx = 0;
     }
     else c = ' ';
     j = u1 + u2 + u2;
#ifdef STDOUT
     printf("|%c|%07d ms\t%c\n",(j==1)?'1':((j==2)?'2':' '),(int)(l*500.0/SAMPLERATE),c);
#endif
     oo2 = o2;
     o2 = u2;
     o1 = u1;
   }
#ifndef STDOUT
   if(a==0 && nx==0)
   {
     mvprintw(22,0,"%s frame %i ",filename,++frame);
     refresh();
     while((clock()-t)*1000LL/CLOCKS_PER_SEC < ms);
     t = clock();
   }
   else refresh();
#endif
 }
#ifndef STDOUT
 getch();
 endwin();
#endif
 return 0;
}


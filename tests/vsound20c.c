#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#define INVERT
#include "shaos30x20.xpm"

double freq[] = {
/*0:*/   110.000000,
/*1:*/   141.274739,
/*2:*/   181.441381,
/*3:*/   233.028034,
/*4:*/   299.281588,
/*5:*/   384.372074,
/*6:*/   493.655131,
/*7:*/   634.009089,
/*8:*/   814.267897,
/*9:*/  1045.777132,
/*10:*/ 1343.108104,
/*11:*/ 1724.974971,
/*12:*/ 2215.412625,
/*13:*/ 2845.289458,
/*14:*/ 3654.250232,
/*15:*/ 4693.211343,
/*16:*/ 6027.565523,
/*17:*/ 7741.297691,
/*18:*/ 9942.271007,
/*19:*/12769.015831,
-1};

unsigned char img[32][20],pal[256];

int main()
{
 int i,j,k,n,fr=44100.0;
 int dx,dy;
 char str[100];
 double d,s;
 struct fre
 {
   int st;
   double cu,pp;
 }*p;
 FILE *f;
 i = j = 0;
 while(1)
 {
   strcpy(str,shaos30x20_xpm[i]);
   if(i==0)
   {
      sscanf(str,"%i %i %i %i",&dx,&dy,&k,&n);
      printf("dx=%i dy=%i col=%i per=%i\n",dx,dy,k,n);
      if(n!=1 || dx!=30 || dy!=20) return -10;
   }
   if(i>=1 && i<k+1)
   {
     n = str[5];
//     printf("%c %c",str[0],n);
     if(n!='o')
     {
        if(n<='9') n-='0';
        else n=n-'A'+10;
        pal[str[0]] = n;
     }
//     printf(" %i\n",n);
   }
   else if(i>=k+1)
   {
     printf("%02d) %s ",j,str);
     for(n=0;n<dx;n++)
     {
        img[n][j] = pal[str[n]];
        printf("%c",img[n][j]<10?img[n][j]+'0':img[n][j]-10+'A');
#ifdef INVERT
        img[n][j] = 15 - img[n][j];
#endif
     }
     img[30][j] = 0;
     printf("\n");
     j++;
   }
   if(++i>=k+1+dy) break;
 }
 n = 0;
 while(freq[n]>0) n++;
 printf("N=%i\n",n);
 p = (struct fre*)malloc(n*sizeof(struct fre));
 if(p==NULL) return -1;
 for(i=0;i<n;i++)
 {
   d = freq[i];
   p[i].cu = 0;
   p[i].st = 0;
   p[i].pp = fr/d/2;
   printf("[%d] %5.2lf (%i...%i) -> %lf\n",i,d,(int)(d-d/10),(int)(d+d/10),p[i].pp);
 }
 s = 32767.0/n;
 f = fopen("vsound.raw","wb");
 if(f==NULL){free(p);return -2;}
 for(j=0;j<44100*5;j++)
 {
    d = 0;
    for(i=0;i<n;i++)
    {
       p[i].cu += 1;
       if((int)(p[i].cu / p[i].pp)&1)
            p[i].st =  1.0;
       else p[i].st = -1.0;
       // 3675 for 12 FPS
       // 4000 for 11 FPS
       // 4410 for 10 FPS
       // 5512 for 8 FPS
       k = j%5512;
       if(k>=666 && k<1000)
       {
          d += s*p[i].st;
       }
       else if(k>=1000 && j>5512)
       {
          d += s*p[i].st/15.0*img[(k-1000)/150][n-i-1];
       }
    }
    k = (int)d;
    fputc(k&255,f);
    fputc((k>>8)&255,f);
 }
 fclose(f);
 free(p);
 return 0;
}

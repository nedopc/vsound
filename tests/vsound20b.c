#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double freq[] = {
/*0:*/   110.000000,
/*1:*/   141.274739,
/*2:*/   181.441381,
/*3:*/   233.028034,
/*4:*/   299.281588,
/*5:*/   384.372074,
/*6:*/   493.655131,
/*7:*/   634.009089,
/*8:*/   814.267897,
/*9:*/  1045.777132,
/*10:*/ 1343.108104,
/*11:*/ 1724.974971,
/*12:*/ 2215.412625,
/*13:*/ 2845.289458,
/*14:*/ 3654.250232,
/*15:*/ 4693.211343,
/*16:*/ 6027.565523,
/*17:*/ 7741.297691,
/*18:*/ 9942.271007,
/*19:*/12769.015831,
-1};

int main()
{
 int i,j,k,n,fr=44100.0;
 double d,s;
 struct fre
 {
   int st;
   double cu,pp;
 }*p;
 FILE *f;
 n = 0;
 while(freq[n]>0) n++;
 printf("N=%i\n",n);
 p = (struct fre*)malloc(n*sizeof(struct fre));
 if(p==NULL) return -1;
 for(i=0;i<n;i++)
 {
   p[i].cu = 0;
   p[i].st = 0;
   p[i].pp = fr/freq[i]/2;
   printf("[%i] %lf\n",i,p[i].pp);
 }
 s = 32767.0/n;
 f = fopen("vsound.raw","wb");
 if(f==NULL){free(p);return -2;}
 for(j=0;j<120000;j++)
 {
    d = 0;
    for(i=0;i<n;i++)
    {
       p[i].cu += 1;
       if((int)(p[i].cu / p[i].pp)&1)
            p[i].st =  1.0;
       else p[i].st = -1.0;
       // 3675 for 12 FPS
       // 4000 for 11 FPS
       if((j>=500 && j<1000) || (j>=4500 && j<5000) ||
          (j>=150*i+1000 && j<150*(i+1)+1000) ||
          (j>=150*(n-i-1)+5000 && j<150*(n-i)+5000)
         )
       {
          d += s*p[i].st;
       }
    }
    k = (int)d;
    fputc(k&255,f);
    fputc((k>>8)&255,f);
 }
 fclose(f);
 free(p);
 return 0;
}

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double freq[] = {
/*0:*/   110.000000,
/*1:*/   141.274739,
/*2:*/   181.441381,
/*3:*/   233.028034,
/*4:*/   299.281588,
/*5:*/   384.372074,
/*6:*/   493.655131,
/*7:*/   634.009089,
/*8:*/   814.267897,
/*9:*/  1045.777132,
/*10:*/ 1343.108104,
/*11:*/ 1724.974971,
/*12:*/ 2215.412625,
/*13:*/ 2845.289458,
/*14:*/ 3654.250232,
/*15:*/ 4693.211343,
/*16:*/ 6027.565523,
/*17:*/ 7741.297691,
/*18:*/ 9942.271007,
/*19:*/12769.015831,
-1};

int main()
{
 int i,j,k,n,fr=44100.0;
 double d,s;
 struct fre
 {
   int st;
   double cu,pp;
 }*p;
 FILE *f;
 n = 0;
 while(freq[n]>0) n++;
 printf("N=%i\n",n);
 p = (struct fre*)malloc(n*sizeof(struct fre));
 if(p==NULL) return -1;
 for(i=0;i<n;i++)
 {
   p[i].cu = 0;
   p[i].st = 0;
   p[i].pp = fr/freq[i]/2;
   printf("[%i] %lf\n",i,p[i].pp);
 }
 s = 127.0/n;
 f = fopen("vsound.raw","wb");
 if(f==NULL){free(p);return -2;}
 for(j=0;j<120000;j++)
 {
    d = 0;
    for(i=0;i<n;i++)
    {
       p[i].cu += 1;
       if((int)(p[i].cu / p[i].pp)&1)
            p[i].st =  1.0;
       else p[i].st = -1.0;
       if(j>=42000 || (j>=2000*(i+1) && j<2000*(i+2)))
       {
          d += s*p[i].st;
       }
    }
    fputc(((int)d)&255,f);
 }
 fclose(f);
 free(p);
 return 0;
}

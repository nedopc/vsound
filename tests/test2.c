#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  int i,j,l,p,lmax=0;
  double o,d,f,s,d1;
  double m = 2;
  double a[256];
  double b = 0;
  double step = 0.0001;
  for(s=0.03;s<1+step;s+=step)
  {
   printf("TRY 2^%lf\n",s);
   m = pow(2.0,s);
   f = 100.0;
   i = 0;
   l = 1200;
   while(f<16000.0)
   {
     printf("%i: %lf\n",i,f);
     for(j=0;j<i;j++)
     {
         o = f/10;
         d = a[j]*3;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*1000.0/f);
             printf(" in 3rd with %i (%1.1f%%)\n",j,p/10.0);
             if(300+p < l) l = 300+p;
         }
         d = a[j]*5;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*1000.0/f);
             printf(" in 5th with %i (%1.1f%%)\n",j,p/10.0);
             if(500+p < l) l = 500+p;
         }
         d = a[j]*7;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*1000.0/f);
             printf(" in 7th with %i (%1.1f%%)\n",j,p/10.0);
             if(700+p < l) l = 700+p;
         }
         d = a[j]*9;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*1000.0/f);
             printf(" in 9th with %i (%1.1f%%)\n",j,p/10.0);
             if(900+p < l) l = 900+p;
         }
         d = a[j]*11;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*1000.0/f);
             printf(" in 11th with %i (%1.1f%%)\n",j,p/10.0);
             if(1100+p < l) l = 1100+p;
         }
     }
     a[i] = f;
     f *= m;
     i++;
   }
   if(l > lmax)
   {
     lmax = l;
     b = s;
     printf("BEST %lf (%i with %i lines)\n",b,l,j+1);
   }
 }
}


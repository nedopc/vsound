#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  int i,j,l,p,lmax=0;
  double o,d,f,s,d1;
  double m = 2;
  double a[256];
  double b = 0;
  for(s=0.03;s<1.001;s+=0.001)
  {
   printf("TRY 2^%lf\n",s);
   m = pow(2.0,s);
   f = 110.0;
   i = 0;
   l = 120;
   while(f<16000.0)
   {
     printf("%i: %lf\n",i,f);
     for(j=0;j<i;j++)
     {
         o = f/10;
         d = a[j]*3;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*100.0/f);
             printf(" in 3rd with %i (%i%%)\n",j,p);
             if(30+p < l) l = 30+p;
         }
         d = a[j]*5;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*100.0/f);
             printf(" in 5th with %i (%i%%)\n",j,p);
             if(50+p < l) l = 50+p;
         }
         d = a[j]*7;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*100.0/f);
             printf(" in 7th with %i (%i%%)\n",j,p);
             if(70+p < l) l = 70+p;
         }
         d = a[j]*9;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*100.0/f);
             printf(" in 9th with %i (%i%%)\n",j,p);
             if(90+p < l) l = 90+p;
         }
         d = a[j]*11;
         if(f >= d-o && f <= d+o)
         {
             p = (int)(fabs(f-d)*100.0/f);
             printf(" in 11th with %i (%i%%)\n",j,p);
             if(110+p < l) l = 110+p;
         }
     }
     a[i] = f;
     f *= m;
     i++;
   }
   if(l > lmax)
   {
     lmax = l;
     b = s;
     printf("BEST %lf (%i with %i lines)\n",b,l,j+1);
   }
 }
}

